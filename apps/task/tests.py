from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User

from apps.task.models import Task

class test_task(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(username='test1', email= 'segovia.jose2196@gmail.com', password= 'tpgcc123')
        user = User.objects.get(username='test1')
        usersinperm = User.objects.create_user(username='test2', email='segovia.jose2196@gmail.com', password='tpgcc123')

    def test_login(self):
        self.assertEqual(self.client.login(
            username='test1', password='tpgcc123'), True)
    
    def test_login_fallo(self):
        self.assertEqual(self.client.login(
            username='test1', password='tpgcc123456'), False)
    
    def test_logout(self):
        self.client.login(username='test1', password='tpgcc123')
        response = self.client.get("/logout/")
        self.assertEqual(response.status_code, 302)

    def test_task_list(self):
        self.client.login(username='test1', password='tpgcc123')
        response = self.client.get('/tasks/list')
        self.assertEqual(response.status_code, 200)



    
        

    



    




