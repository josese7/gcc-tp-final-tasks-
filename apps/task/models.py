from django.db import models


class Task(models.Model):
    nombre = models.CharField(max_length=50)
    detalles = models.TextField()
    horas = models.IntegerField()
