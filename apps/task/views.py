from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, CreateView
from apps.task.models import Task
from apps.task.forms import TaskForm
from django.urls import reverse_lazy

# Create your views here.


def index(request):
    return render(request, 'homepage.html')


""" def taskList(request):
    tasks =    Task.objects.all()
    contexto =  {'tasks' :tasks}
    return render( request, ) """
class TasksList(ListView):
    model = Task
    template_name = 'tasks/task-list.html'

class TasksCreate(CreateView):
    model = Task
    form_class = TaskForm
    template_name = 'tasks/task-create.html'
    success_url="/tasks/list"
    
