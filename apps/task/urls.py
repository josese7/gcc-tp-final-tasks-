from django.urls import include, path
from apps.task.views import index, TasksList, TasksCreate
from django.contrib.auth.decorators import login_required



urlpatterns = [
    path('', login_required(index), name='homepage'),
    path('list', login_required(TasksList.as_view()), name='task_list'),
    path('create', login_required(TasksCreate.as_view()), name='task-create')

]
