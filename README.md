# TP-GCC

<h3>PROYECTO DE GESTOR DE TAREAS</h3>
<h4>con heroku y postgres </h4>
<h3>Trabajo práctico para Gestión de Centro de Cómputos</h3>
<h4>FP-UNA 2019</h4>
<h4>JOSE MANUEL SEGOVIA MENDOZA</h4>

<div>
    Al principio, se ejecuta un script llamado before_script, que muestra la versión de python e instala 
    las dependencias de la aplicacion que estan anotadas en requirements.txt con pip install -r requirements.txt
</div>

<strong>Desarrollo en:</strong> https://app-gcctp1.herokuapp.com/accounts/login/

Fases:
<ul>
    <div><li>Verificacion: compila la aplicación con python manage.py check y verifica que no haya errores. Posteriormente se ejecuta el pip freeze para listar los paquetes instalados en el entorno virtual</li></div>
    <div><li>PruebasUnitarias: se ejecutan las 4 pruebas( login, login_fallido, logut, listar tareas )</li></div>
    <div><li>DesarrolloDp: Se instala las dependencias necesarias para deployar la app en heroku y luego se deplolya</li></div>
</ul>
Usuarios:
<div><ul>
    <div><li>josese</li></div>
    <div><li>contraseña: tpgcc123</li></div><div> </div>
</ul></div>

<strong>Produccion en:</strong> https://app-gcctp2.herokuapp.com/accounts/login/

Fases:
<ul>
    <div><li>Verificacion: compila la aplicación con python manage.py check y verifica que no haya errores. Posteriormente se ejecuta el pip freeze para listar los paquetes instalados en el entorno virtual</li></div>
    <div><li>PruebasUnitarias: se ejecutan las 4 pruebas( login, login_fallido, logut, listar tareas )</li></div>
    <div><li>DesarrolloDp: Se instala las dependencias necesarias para deployar la app en heroku y luego se deplolya</li></div>
</ul>
Usuarios:
<div><ul>
    <div><li>ramiro</li></div>
    <div><li>contraseña: tpgcc123</li></div><div> </div>
</ul></div>
<div>
<strong>Paths disponibles : </strong>
    admin/ --> admin de Django
    tasks/ --> HomePage
    tasks/list --> Listado de tareas
    tasks/create --> Agregar tarea
    accounts/login/ --> Iniciar sesión
    logout/         --> Cerrar sesión
</div>

<strong> Para agregar usuarios, se debe acceder a la consola de la app y escribir : bash -> python manage.py createsuperuser </strong>
<strong> O desde el admin </strong>
